<?php

namespace App;

use App\Events\CommentCreated;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{   protected $fillable=['body','thread_id','user_id'];


    public function thread(){

        return $this->belongsTo(Thread::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
