<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AdminCreationController extends Controller
{

    public function edit()
    {
        return view('top-nav.make_admin');
    }

    public function update()
    {
        $validate = request()->validate([

            'email'    => ['required', 'exists:users,email,role,0']
        ]);

        $user=User::getUserByEmail(request(['email']))->first();

        $user->role=1;
        $user->save();
        return redirect("/");

    }
}
