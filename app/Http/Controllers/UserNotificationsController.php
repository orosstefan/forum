<?php

namespace App\Http\Controllers;

use App\Listeners\SendCommentCreatedNotification;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;


class UserNotificationsController extends Controller
{
    public function index()
    {

                return view('notifications');
    }
    public function update(DatabaseNotification $notification)
    {
                $notification->markAsRead();
                return redirect('/notifications');
    }
}
