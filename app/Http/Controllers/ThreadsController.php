<?php

namespace App\Http\Controllers;

use App\Notifications\ThreadCreated;
use App\Thread;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ThreadsController extends Controller
{


    public function index()
    {   $threads=Thread::all();

                return view('threads.index',compact('threads'));
    }


    public function create()
    {
        return view('threads.create');
    }


    public function store()
    {
        $validated=$this->validateThread();

        $validated['user_id']=auth()->id();

      $thread=Thread::create($validated);
      Notification::send( User::all()->except(Auth::id()),new ThreadCreated($thread));
        return redirect('/');
    }


    public function show(Thread $thread)
    {
        return view('threads.show',compact('thread'));
    }


    public function edit(Thread $thread)
    {
        return view('threads.edit',compact('thread'));
    }


    public function update(Request $request, Thread $thread)
    {

        $this->validateThread();
        $thread->update(request(['title','description']));
        return redirect("/threads/{$thread->id}");
    }

    public function destroy(Thread $thread)
    {
        $thread->delete();
        return redirect('/');
    }

    protected function validateThread(){
        return request()->validate([
            'title'=>['required','min:3'],
            'description'=>['required','min:3']

        ]);
    }
}
