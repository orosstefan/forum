<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentCreated;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ThreadCommentsController extends Controller
{
    public function create()
    {
           return view('comments.create',["thread_id"=>request()->thread_id]);
}

    public function store()
    {
        $validated=$this->validateComment();

        $validated['thread_id']=request()->thread_id;


        $validated['user_id']=Auth::id();
        $comment=Comment::create($validated);

        if($comment->thread->user->id!==$comment->user_id)
           { event(new CommentCreated($comment));
           }
      return redirect()->action('ThreadsController@show', ['thread' => request()->thread_id]);
    }

    public function edit(Comment $comment)
    {   $this->authorize('update',$comment);
        return view('comments.edit',compact('comment'));
    }
    public function update( Comment $comment)
    {
        $this->authorize('update',$comment);
        $this->validateComment();
        $comment->update(request(['body']));
        return redirect("/threads/{$comment->thread_id}");
    }

    public function destroy(Comment $comment)
    {   $tmp_thread_id=$comment->thread_id;
        $comment->delete();
        return redirect("/threads/{$tmp_thread_id}");
    }

    protected function validateComment(){
        return request()->validate([
            'body'=>['required','min:3']

        ]);
    }
}
