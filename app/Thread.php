<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $fillable=['title','description','user_id'];
    public static function boot()
    {
        parent::boot();
        static::deleting(function($thread) {

                $thread->comments()->get()
                    ->each(function($comment) {
                        $comment->delete();
                    });
        });


    }
    public function user(){

        return $this->belongsTo(User::class);
     }

     public function comments(){

        return $this->hasMany(Comment::class);
     }

}
