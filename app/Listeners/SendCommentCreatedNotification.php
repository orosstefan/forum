<?php

namespace App\Listeners;

use App\Events\CommentCreated;
use App\Mail\CommentCreated as CommentCreatedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendCommentCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentCreated  $event
     * @return void
     */
    public function handle(CommentCreated $event)
    {
        Mail::to($event->comment->user->email)->queue(
            new CommentCreatedMail($event->comment)
        );
    }
}
