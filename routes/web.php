<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'ThreadsController@index');


Route::group(['prefix' => 'threads' ], function(){
    //admin user
    Route::group(['middleware' => ['auth','admin']], function(){

        Route::get('{thread}/edit','ThreadsController@edit');
        Route::patch('{thread}','ThreadsController@update');
        Route::delete('{thread}','ThreadsController@destroy');

    });
    //normal user
    Route::group(['middleware' => ['auth']], function(){

        Route::get('create','ThreadsController@create');
        Route::post('','ThreadsController@store');
    });
    //guest
    Route::get('{thread}','ThreadsController@show');


});
Route::group(['prefix'=>'comments'],function(){

    //admin user
    Route::group(['middleware'=>['auth','admin']], function () {
        Route::delete('{comment}','ThreadCommentsController@destroy');
    });
    //normal user
    Route::group(['middleware'=>['auth']], function () {
        Route::get('create', 'ThreadCommentsController@create');
        Route::post('','ThreadCommentsController@store');
        Route::get('{comment}/edit','ThreadCommentsController@edit');
        Route::patch('{comment}','ThreadCommentsController@update');
    });
    //guest

});

Route::group(['prefix'=>'admin-creation','middleware'=>['auth','admin']],function(){

    Route::get('', 'AdminCreationController@edit');
    Route::post('', 'AdminCreationController@update');
});

Route::group(['prefix'=>'notifications','middleware'=>['auth']],function(){

    Route::get('','UserNotificationsController@index');
    Route::patch('{notification}','UserNotificationsController@update');
});

Auth::routes();






