<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => 'oros.stefan@yahoo.com',
            'role'=>1,
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => 'oros.stefan@yahoo2.com',
            'role'=>0,
            'password' => bcrypt('password'),
        ]);
        factory(App\User::class, 5)->create();
    }
}
