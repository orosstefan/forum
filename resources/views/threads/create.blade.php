@extends('layouts/app')



@section('content')


    <h1>Create a new thread</h1>
    <form method="POST" action="/threads">
        @csrf
        <div class="form-group">
            <label for="threadTitle">Thread title</label>
            <input id="threadTitle" type="text" class="form-control" name="title" placeholder="Thread title" value="{{old('title')}}">
        </div>

        <div class="form-group">
            <label for="threadDescription">Thread description</label>
            <textarea id="threadDescription" name="description" placeholder="Thread description" class="form-control" >{{old('description')}}</textarea>

            </div>

            <button type="submit" class="btn btn-primary">Create project</button>


    </form>
    @include('errors')

@endsection
