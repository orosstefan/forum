@extends('layouts/app')

@section('content')
<h1 class="title">Edit thread</h1>

<form method="POST" action="/threads/{{$thread->id}}">
    @method("PATCH")
    @csrf
    <div class="form-group">
        <label class="label" for="threadTitle">Title</label>

        <input id="threadTitle" type="text" class="form-control" name="title" placeholder="Title"
            value="{{$thread->title}}">

    </div>

    <div class="form-group">
        <label class="label" for="threadDescription">Description</label>


        <textarea id="threadDescription" name="description" placeholder="Description" class="form-control">{{$thread->description}}</textarea>

    </div>


    <button type="submit" class="btn btn-outline-primary">Update thread</button>



</form>
@include('errors')


@endsection
