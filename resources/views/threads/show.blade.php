@extends('layouts/app')

@section('content')
<div class="card-block card p-3 m-4" >
    <div class="row">


        <div class="col-md-9">
            <blockquote class="blockquote">


                <h1 class='title'>{{$thread->title}}</h1>

                <small>
                    <footer class="blockquote-footer">Started by <cite title="author">{{$thread->user->name}},
                            {{date("Y-m-d h:i",strtotime($thread->created_at))}}</cite></footer>
                </small>
            </blockquote>
            <p>{{$thread->description}}</p>
            @if (Auth::check())

            <form method="GET" action="/comments/create">

                @csrf
                <input type="hidden" name="thread_id" value="{{ $thread->id }}">


                    <button type="submit" class="btn btn-outline-primary">Reply to thread</button>


            </form>
            @endif
        </div>
        <div class="col-md-3  ">

            @if(Auth::check()&& Auth::user()->isAdmin())



            <form method="POST" action="/threads/{{$thread->id}}">
                @method('DELETE')
                @csrf

                <button type="submit" class="btn btn-outline-danger float-right ml-1">Delete thread</button>


            </form>
            <a class="btn btn-outline-primary float-right" href="/threads/{{$thread->id}}/edit">Edit</a>
            @endif

        </div>

    </div>
</div>

@include('comments/index')

@endsection
