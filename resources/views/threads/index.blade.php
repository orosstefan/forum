
@extends('layouts/app')
@section('content')
<h1>Threads</h1>




    <div class="row">
        <div class="col-lg-8">
                @foreach ($threads as $thread)
            <div class="card p-3 m-4">

                <blockquote class="blockquote">
                    <a href="/threads/{{$thread->id}}" >
                        {{$thread->title}}
                    </a>
                     <small><footer class="blockquote-footer">Started by <cite title="author">{{$thread->user->name}}, {{date("Y-m-d h:i",strtotime($thread->created_at))}}</cite></footer></small>
                </blockquote>
                <p>
                    {{$thread->description}}
                </p>
                <p class="text-right text-muted">{{$thread->comments->count()}} comments</p>
            </div>
            @endforeach
        </div>
        <div class="col-lg-4  ">
            <div class="d-flex justify-content-sm-center">
                @if (Auth::check())

                <a href="/threads/create" class="btn btn-outline-primary">Start New Discussion</a>
            @endif
        </div>
        </div>
    </div>




@endsection


