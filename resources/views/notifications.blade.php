@extends('layouts/app')

@section('content')
@if(Auth::check())


@foreach ( Auth::user()->unreadNotifications as $notification)

<div class="card-block card p-3 m-4">
    <div class="row">
        <div class="col-md-9"> A new discussion about <mark>{{$notification->data['title']}}</mark> has been started.
        </div>
        <div class="col-md-3">
            <form method="POST" action="/notifications/{{$notification->getKey()}}">
                @method("PATCH")
                @csrf
                <button type="submit" class="btn btn-outline-primary">Mark as read</button>
            </form>
        </div>
    </div>
</div>
    @endforeach
    @endif
    @endsection
