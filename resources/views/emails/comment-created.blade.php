@component('mail::message')
Hello,

Someone has commented on {{$comment->thread->title}}


@component('mail::button', ['url' => 'http://127.0.0.1:8000/threads/' . $comment->thread->id])
Go there
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
