
@if ($thread->comments->count())

    @foreach($thread->comments as $comment)


    <div class="card-block card p-3 m-4">
            <div class="row">


                    <div class="col-md-9">
    <blockquote class="blockquote">

            <h3 class='title'>{{$comment->user->name}}</h3>

            <small>
                <footer class="blockquote-footer">Posted at <cite title="date">
                        {{date("Y-m-d h:i",strtotime($comment->created_at))}}</cite></footer>
            </small>

    </blockquote>
    <p>{{$comment->body}}</p>

            </div>
            <div class="col-md-3">
                    @can('update', $comment)
                    <a href="/comments/{{$comment->id}}/edit" class="btn btn-outline-primary float-right">Modify</a>
                    @endcan
            </div>
    </div>
</div>

    @endforeach

@endif
