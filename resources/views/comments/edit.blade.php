@extends('layouts/app')



@section('content')
    <h1>Modify comment</h1>
<form method="POST" action="/comments/{{$comment->id}}" class=" pb-1">
    @method('PATCH')
    @csrf


    <div class="form-group">
        <label for="comBody">Your comment</label>
        <textarea id="comBody" name="body" placeholder="Comment text" class="form-control" >{{$comment->body}}</textarea>
        </div>

        <button type="submit" class="btn btn-outline-primary custom-length">Save changes</button>
</form>
@if(Auth::check()&& Auth::user()->isAdmin())
<form method="POST" action="/comments/{{$comment->id}}">
    @method('DELETE')
    @csrf

        <button type="submit" class="btn btn-outline-danger custom-length">Delete</button>


</form>
@include('errors')
@endif
@endsection


