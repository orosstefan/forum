@extends('layouts/app')



@section('content')
<h1>Reply to thread</h1>
<form method="POST" action="/comments">
    @csrf
    <div class="form-group">

        <input class="form-control" type="hidden" name="thread_id" value="{{ $thread_id }}">
    </div>

    <div class="form-group">
        <label for="comBody">Your comment</label>
        <textarea id="comBody" name="body" placeholder="Comment text" class="form-control">{{old('body')}}</textarea>
    </div>

    <button type="submit" class="btn btn-outline-primary">Comment</button>


</form>
@include('errors')
@endsection
