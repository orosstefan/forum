
@extends('layouts/app')

@section('content')



<h1>Make admin by email</h1>
<form method="POST" action="/admin-creation">

@csrf
    <div class="form-group">
      <label for="email">Email address</label>
      <input type="email" class="form-control" name="email" placeholder="name@example.com">
    </div>
    <button type="submit" class="btn btn-primary">Make admin</button>
  </form>
  @include('errors')
@endsection

